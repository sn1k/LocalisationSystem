﻿using System;

using NUnit.Framework;

public class BasicTests
{
	public void RunTests(ILocalisationFileLoader in_fileLoader, ILocalisationLogger in_logger)
	{
		ILocalisationController m_localisationController = new LocalisationController(in_fileLoader, in_logger);
		
		m_localisationController.SetLanguage(Localisation.SupportedLanguage.en_GB);
		RunValidLangTest(m_localisationController);
		
		m_localisationController.SetLanguage(Localisation.SupportedLanguage.es_ES);
		RunValidLangTest(m_localisationController);

		m_localisationController.SetLanguage(Localisation.SupportedLanguage.UNSET);
		RunInvalidLangTest(m_localisationController);
	}

	private void RunValidLangTest(ILocalisationController in_localisationController)
	{
		string connectingString = in_localisationController.GetKey("CONNECTING");
		string loadingString = in_localisationController.GetKey("LOADING");
		string guestString = in_localisationController.GetKey("GUEST");
		string invalidString = in_localisationController.GetKey("INVALID");

		bool test0Success = (connectingString != LocalisationDataFormatInfo.k_notFoundValue);
		bool test1Success = (loadingString != LocalisationDataFormatInfo.k_notFoundValue);
		bool test2Success = (guestString != LocalisationDataFormatInfo.k_notFoundValue);
		bool test3Success = (invalidString == LocalisationDataFormatInfo.k_notFoundValue);

		Assert.That(test0Success, Is.EqualTo(true));
		Assert.That(test1Success, Is.EqualTo(true));
		Assert.That(test2Success, Is.EqualTo(true));
		Assert.That(test3Success, Is.EqualTo(true));
	}

	private void RunInvalidLangTest(ILocalisationController in_localisationController)
	{
		string connectingString = in_localisationController.GetKey("CONNECTING");

		bool test0Success = (connectingString == LocalisationDataFormatInfo.k_notFoundValue);

		Assert.That(test0Success, Is.EqualTo(true));
	}
}
