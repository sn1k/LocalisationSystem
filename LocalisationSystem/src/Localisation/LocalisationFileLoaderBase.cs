﻿
/// <summary>
/// Interface describes signature for language file loading, allows extraction of unity-dependant code
/// </summary>
public interface ILocalisationFileLoader
{
	string LoadLocalisationFile(string in_languagePath);
}
