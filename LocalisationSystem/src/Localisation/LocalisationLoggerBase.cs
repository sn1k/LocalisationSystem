﻿
/// <summary>
/// Interface describes signature for logging
/// </summary>
public interface ILocalisationLogger
{
	void DebugLog(string in_message);
}
