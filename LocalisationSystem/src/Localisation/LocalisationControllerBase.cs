﻿
namespace Localisation
{
	public enum SupportedLanguage { en_GB, es_ES, fr_FR, de_DE, pl_PL, UNSET };
}

public static class LocalisationDataFormatInfo
{
	public static readonly string k_notFoundValue = "<MISSING>";

	public static char k_keyValSeparator = ':';
}

/// <summary>
/// Interface describes contract for controller that manages loading and access of language files
/// </summary>
public interface ILocalisationController
{
	Localisation.SupportedLanguage GetLanguage();
	bool SetLanguage(Localisation.SupportedLanguage in_language);

	string GetKey(string in_key);
}
