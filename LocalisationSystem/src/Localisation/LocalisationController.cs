﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

using Localisation;

/// <summary>
/// Class handles loading and access into key/val language files
/// </summary>
public class LocalisationController : ILocalisationController
{
	private Dictionary<string, string> m_languageMap;

	private ILocalisationFileLoader m_localisationUtils;
	private ILocalisationLogger m_logger;

	private SupportedLanguage m_selectedLanguage = SupportedLanguage.UNSET;
	
	const string k_languageFileExtension = ".txt";


	/// <summary>
	/// Constructor, takes a platform utility dependancy
	/// </summary>
	/// <param name="in_platformUtils">Utils handle platform specific file loading and logging</param>
	public LocalisationController(ILocalisationFileLoader in_platformUtils, ILocalisationLogger in_logger)
	{
		m_localisationUtils = in_platformUtils;
		m_logger = in_logger;
	}	

	/// <summary> Loads requested language set if supported and not already loaded </summary>
	/// <param name="in_language"></param>
	/// <returns>false if could not load language, true otherwise.</returns>
	public bool SetLanguage(SupportedLanguage in_language)
	{
		if(in_language == m_selectedLanguage)
		{
			m_logger.DebugLog("LocalisationController : SetLanguage requested already loaded language set");
			return true;
		}

		if(m_languageMap != null)
			m_languageMap.Clear();
		
		if (in_language != SupportedLanguage.UNSET)
		{
			m_languageMap = LoadLanguageSet(in_language);

			if (m_languageMap.Count == 0)
			{
				m_selectedLanguage = SupportedLanguage.UNSET;
				m_logger.DebugLog("LocalisationController : SetLanguage loaded empty set for language " + in_language);
				return false;
			}
		}
		
		m_selectedLanguage = in_language;
		return true;
	}

	/// <returns>UNSET if nothing loaded, the loaded language type otherwise</returns>
	public SupportedLanguage GetLanguage()
	{
		if (m_languageMap == null || m_languageMap.Count == 0)
			return SupportedLanguage.UNSET;

		return m_selectedLanguage;
	}

	/// <param name="in_key">sentence/word key</param>
	/// <returns>currently loaded value for requested key, the unset-value if not found</returns>
	public string GetKey(string in_key)
	{
		if (m_languageMap == null || m_languageMap.ContainsKey(in_key) == false)
			return LocalisationDataFormatInfo.k_notFoundValue;

		return m_languageMap[in_key];
	}

	/// <summary> Determines language file path and requests loading of the dictionary </summary>
	/// <param name="in_language"></param>
	/// <returns>dictionary of keyvals</returns>
	private Dictionary<string, string> LoadLanguageSet(SupportedLanguage in_language)
	{
		string languageName = in_language.ToString();
		
		return LoadLanguageFile(languageName + k_languageFileExtension);
	}

	/// <summary> Loads a language file in format key:val\n </summary>
	/// <param name="in_path"></param>
	/// <returns>dictionary of keyvals</returns>
	private Dictionary<string, string> LoadLanguageFile(string in_path)
	{
		Dictionary<string, string> languageMap = new Dictionary<string, string>();
		
		string fileContents = m_localisationUtils.LoadLocalisationFile(in_path);

		if (fileContents == string.Empty)		
			return languageMap;		

		using (StringReader file = new StringReader(fileContents))
		{
			while (true)
			{
				string lineStr = file.ReadLine();

				if (lineStr == null)
					break;

				if (lineStr == string.Empty)
					continue;

				int separatorIndex = lineStr.IndexOf(LocalisationDataFormatInfo.k_keyValSeparator);

				if (separatorIndex == -1)
				{
					m_logger.DebugLog(String.Format("{0}{1}{2}{3}", "LoadLanguageFile ", in_path, " missing value in ", lineStr));
					continue;
				}

				string key = lineStr.Substring(0, separatorIndex);
				string val = lineStr.Substring(separatorIndex + 1);
				
				languageMap[key] = val;
			}
		}

		return languageMap;
	}
}
