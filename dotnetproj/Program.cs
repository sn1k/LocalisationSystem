﻿using System;

using NUnit.Framework;


[TestFixture]
class Program
{		
	static void Main(string[] args)
	{
	}

	[Test]
	public void RunTests()
	{
		BasicTests basicTests = new BasicTests();
		ILocalisationLogger logger = new LocalisationLoggerDotNet();
		basicTests.RunTests(new LocalisationFileLoaderDotNet(logger), logger);
	}
}

