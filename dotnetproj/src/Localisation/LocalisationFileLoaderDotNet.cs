﻿using System.IO;
using System;

public class LocalisationFileLoaderDotNet : ILocalisationFileLoader
{
	private static string k_localisationFilesDirectory = "Localisation/";

	private ILocalisationLogger m_logger;


	public LocalisationFileLoaderDotNet(ILocalisationLogger in_logger)
	{
		m_logger = in_logger;
	}

	public string LoadLocalisationFile(string in_languagePath)
	{
		string lines = string.Empty;

		try
		{
			lines = File.ReadAllText(k_localisationFilesDirectory + in_languagePath);
		}
		catch (Exception e)
		{
			m_logger.DebugLog("LocalisationFileLoaderDotNet Error opening file at path: " + in_languagePath + ", " + e.ToString());
			return string.Empty;
		}

		if (lines == string.Empty)
			m_logger.DebugLog("LocalisationFileLoaderDotNet File was blank: " + in_languagePath);

		return lines;
	}
}
