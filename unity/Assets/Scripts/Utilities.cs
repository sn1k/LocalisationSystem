﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Assets.Engine.Consts;
using UnityEngine;

public class CUtilities
{
    // Delegate info depending on Build ENV.
    internal static TargetEnvironment TARGET_ENVIRONMENT
    {
        get { return TargetEnvironment.Test; }
    }
    internal static string TARGET_BUILD_VERSION
    {
        get { return ""; }
    }
    internal static string TARGET_SVNREVISION
    {
        get { return ""; }
    }
    internal static string PATCH_SERVER_ROOT
    {
        get { return ""; }
    }
    internal static int INTERNAL_APP_VERSION
    {
        get { return 1; }
    }


    public static void logDebug(string str)
    {
        if (TargetEnvironment.Test != TARGET_ENVIRONMENT)
        {
            return;
        }

        Debug.Log("db " + str);
    }

    public static void logDebugWarning(string str)
    {
        if (TargetEnvironment.Test != TARGET_ENVIRONMENT)
        {
            return;
        }

        Debug.LogWarning("db " + str);
    }

    public static void logDebugError(string str)
    {
        if (TargetEnvironment.Test != TARGET_ENVIRONMENT)
        {
            return;
        }

        Debug.LogError("db " + str);
    }
    
}
