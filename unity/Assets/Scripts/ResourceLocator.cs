using System;
using System.IO;
using System.Net.Mime;
using Assets.Engine.Consts;
using UnityEngine;

public class ResourceLocator
{
    public static UnityEngine.Object LoadResourceImmediate(Type resourceType, string fileNameWithPath)
    {
        UnityEngine.Object ret = null;
        string path = "";
        string filename = fileNameWithPath;
        string ext = "";

        if (fileNameWithPath.LastIndexOf("/") > -1)
        {
            filename = fileNameWithPath.Substring(fileNameWithPath.LastIndexOf("/") + 1);
            path = fileNameWithPath.Substring(0, fileNameWithPath.LastIndexOf("/"));

        }
        if (filename.LastIndexOf(".") > -1)
        {
            ext = filename.Substring(filename.LastIndexOf(".") + 1);
            filename = filename.Substring(0, filename.LastIndexOf("."));
        }

        string load = String.Format("{0}/{1}", path, filename);
        ret = Resources.Load(load, resourceType);
        return ret;
    }

    internal static void UnloadUnusedResources()
    {
        //Resources.UnloadUnusedAssets();
    }

    /// <summary>
    /// Load patched asset, using the Texture2D naming convention, from persistent storage 
    /// </summary>
    /// <param name="in_fileName"></param>
    /// <returns>texture if found, null otherwise</returns>
    public static Texture2D LoadAssetTexture(string in_fileName)
    {
        var bundleName = "Texture2D_" + in_fileName + ".unity3d";
        return LoadPatchedAsset(bundleName) as Texture2D;
    }


    public static GameObject LoadParticlesObject(string particlePath)
    {
        return (GameObject)Resources.Load(particlePath);
    }



    private static GameObject LoadPrefab(string in_filename)
    {
        

        return null;
    }


	internal static GameObject LoadPetPrefab(string fileName)
	{
        var objToReturn = LoadPrefab(fileName);

        if (objToReturn != null)
            return objToReturn;
       
		//default to the original
		return Resources.Load<GameObject>("prefabCompanions/" + fileName);     
	}

    internal static GameObject LoadMountPrefab(string fileName)
    {
        var objToReturn = LoadPrefab(fileName);

        if (objToReturn != null)
            return objToReturn;

        //default to the original
        return Resources.Load<GameObject>("Mounts/" + fileName);  
    }

    internal static GameObject LoadSaddlePrefab(string fileName)
    {
        var objToReturn = LoadPrefab(fileName);

        if (objToReturn != null)
            return objToReturn;

        //default to the original
        return Resources.Load<GameObject>("Saddles/" + fileName);
    }


	public static TextAsset LoadLocalisationAssetImmediately(string in_localisationAsset)
	{
		// TODO manual path building is dodgy, MS only support '/' because they're nice, another platform might not
		string fileNameWithPath = String.Format("{0}/{1}", Constants.LOCALISATION_FILES_DIRECTORY, in_localisationAsset);
		return (TextAsset)LoadResourceImmediate(typeof(TextAsset), fileNameWithPath);
	}


    public static TextAsset LoadTextAssetImmediately(string textAssetName)
    {
        string textAssetNameWithoutPath = textAssetName.Replace("conversation_xml/", "");
        if (textAssetName.Contains("conversation_xml/"))
        {
            textAssetName = textAssetNameWithoutPath;
        }
        
        
            string fileNameWithPath = String.Format("{0}/{1}", Constants.TEXT_FILES_DIRECTORY, textAssetName);
            return (TextAsset) LoadResourceImmediate(typeof (TextAsset), fileNameWithPath);
        
    }

    private static UnityEngine.Object LoadPatchedAsset(string fileName)
    {
        var assetBundle = AssetBundle.CreateFromFile(Application.persistentDataPath + "//" + fileName);
        if (assetBundle == null)
            return null;

        var asset = assetBundle.mainAsset;
        assetBundle.Unload(false);
        return asset;
    }

    private static TextAsset LoadPatchedTextFile(string textAssetName)
    {
        var fileNameWithoutExtention = Path.GetFileNameWithoutExtension(textAssetName);

        string assetBundleName = String.Format("{0}" + fileNameWithoutExtention + "{1}", "TextAsset_", ".unity3d");
        var bundle = AssetBundle.CreateFromFile(Application.persistentDataPath + "//" +assetBundleName);

        if (bundle == null)
            return null;

        var textAsset = bundle.mainAsset as TextAsset;
        bundle.Unload(false);
        return textAsset;
    }

    public static string GetTextDBPatchPath()
    {
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            return Application.persistentDataPath + "/text.db";
        }

        return Application.streamingAssetsPath + "/text.db";
    }

    public static string GetTextDBPath()
    {
        if (Application.platform == RuntimePlatform.Android)
            return Application.persistentDataPath + "/text.db";

        return Application.streamingAssetsPath + "/text.db";
    }

    public static string GetPatchPath()
    {
        return Application.persistentDataPath + "/";
    }

    public static string GetDataPath()
    {
        return Application.dataPath;
    }

    
}
