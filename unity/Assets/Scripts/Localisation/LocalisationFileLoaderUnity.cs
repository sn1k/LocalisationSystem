﻿using System.IO;
using System;

using UnityEngine;


public class LocalisationFileLoaderUnity : ILocalisationFileLoader
{
	private ILocalisationLogger m_logger;

	public LocalisationFileLoaderUnity(ILocalisationLogger in_logger)
	{
		m_logger = in_logger;
	}

	public string LoadLocalisationFile(string in_languagePath)
	{
		TextAsset asset = ResourceLocator.LoadLocalisationAssetImmediately(in_languagePath);

		if (asset == null)
		{
			m_logger.DebugLog("LocalisationFileLoaderUnity couldn't open asset at " + in_languagePath);
			return string.Empty;
		}

		return asset.text;
	}
}
