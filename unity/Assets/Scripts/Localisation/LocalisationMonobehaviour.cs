﻿//using UnityEngine;

using NUnit.Framework;


[TestFixture]
public class LocalisationMonobehaviour //: MonoBehaviour
{
	[Test]
	public void RunTests()
	{
		BasicTests basicTests = new BasicTests();
		ILocalisationLogger logger =  new LocalisationLoggerUnity();
		ILocalisationFileLoader loader =  new LocalisationFileLoaderUnity(logger);
		basicTests.RunTests(loader, logger);
	}
}
