﻿using System.IO;
using System;

using UnityEngine;


public class LocalisationLoggerUnity : ILocalisationLogger
{
	public void DebugLog(string in_message)
	{
		CUtilities.logDebug(in_message);
	}
}
