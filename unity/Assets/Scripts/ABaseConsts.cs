﻿using System;

namespace Assets.Engine.Consts
{
    public static class Constants
    {
		public static readonly String LOCALISATION_FILES_DIRECTORY = "Localisation";
        public static readonly String TEXT_FILES_DIRECTORY = "TextFiles";
        public static readonly String STRINGS_ID_DIRECTORY = "strings_for_id";

        public static readonly String MASTER_ASSETS_DIRECTORY = "Master Assets";
        public static readonly String ANIMATION_DIRECTORY = "Animations";
        public static readonly String MESHES_DIRECTORY = "Meshes";
        public static readonly String TEXTURES_DIRECTORY = "Textures";
        public static readonly String ICONS_DIRECTORY = "Resources/Icons";

        public static readonly String APPSTORE_TRANSID = "APPSTORE_TRANSID";
        public static readonly String APPSTORE_PRODUCT = "APPSTORE_PRODUCT";
        public static readonly String APPSTORE_RECEIPT = "APPSTORE_RECEIPT";
    }
}